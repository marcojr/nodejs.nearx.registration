var express = require('express');
var http = require('http');
var path = require('path');
var app = express();
var md5 = require('MD5');
global.config = require('./config.json');
var mongojs = require('mongojs');
var ObjectId = mongojs.ObjectId;
global.db = require('mongojs').connect(config.mongoDB, config.mongoDBcollections);
global.uuid = require('uuid');
global.s3 = require('aws2js').load('s3', config.awsAccessKeyId, config.awsSecretAccessKey);
global.s3.setBucket('nearx');
global.smsProviderCredentials = {
    key : config.nexmoKey,
    secret : config.nexmoSecret
}
// all environments
app.set('port', process.env.PORT || 5000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
//app.use(allowCrossDomain);
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

var register = require('./routes/register');
var aux = require('./routes/aux');

// development only
if ('development' == app.get('env')) {
    app.use(express.errorHandler());
}

app.get('/aux/:subject/:data', aux.query);
app.post('/register/:Id', register.create);
app.post('/register/:Id/smsConfirm', register.confirmSms);
app.post('/register/:Id/login', register.login);
app.post('/register/:Id/resetPassword', register.resetPassword);
app.get('/register/:Id/getSms', register.getSms);



/* Code to generate countries list - uncomment to generate
var countries = [];
var find = {
    'type': 'codes'
}
global.db.people.find(find, function (err, item) {
    for(var i=0; i < item[0].codes.length; i++){
       // console.log(item[0].codes[i].iso);
        var reg = {
            type : 'country',
            iso : item[0].codes[i].iso.toString().trim(),
                name : item[0].codes[i].country,
                areaCode : item[0].codes[i].areaCode.toString().trim(),
                population: item[0].codes[i].population,
                area: item[0].codes[i].area
        }
        //var jsonstr = JSON.stringify(reg);
        //jsonstr = jsonstr.replace(/"key":/g, '"' + item[0].codes[i].iso + '":');
        //var new_obj = JSON.parse(jsonstr);
        global.db.aux.insert(reg);
    }
});
return;
*/
http.createServer(app).listen(app.get('port'), function (req, res) {
    console.log('Express server listening on port ' + app.get('port'));
});