var Q = require('q');
exports.create = function (req, res) {
    var doCreate = function () {
        var id = req.param("Id");
        var data = req.body;
        var systemId = global.uuid.v4();
        var sms = Math.floor(Math.random() * 9000) + 1000;
        var dte = new Date();
        var model = {
            id: id,
            systemId: systemId,
            password: data.password,
            name: data.name,
            country: 'uk',
            iac: data.iac,
            source: "app",
            appVersion: data.appVersion,
            dateCreation: dte,
            lastLogin: null,
            status: "sms",
            smsConfirmation: sms,
            appSettings: {

            },
            device: {
                platform: data.platform,
                version: data.version,
                manufacturer: data.manufacturer,
                model: data.model,
                wDisplay: data.wDisplay,
                hDisplay: data.hDisplay
            },
            contacts: data.contacts
        }
        global.db.people.find({
            'id': id
        }, function (err, item) {
            if (item.length == 0) {
                addNewRecord(model, res, req, data.picture);
            } else {
                if (item[0].status == 'sms') {
                    sendSms(sms, id, res);
                } else {
                    log("cannot create account " + id + " because it's already exists", 400, req.connection.remoteAddress, false);
                    res.send(400);
                }
            }
        });
    }
    isIpBlocked(req.connection.remoteAddress)
        .then(doCreate, function () {
            res.send(403);
        });
};
exports.confirmSms = function (req, res) {
    var doConfirm = function () {
        var id = req.param("Id");
        var data = req.body;
        var sms = data.sms;
        var pass = data.password;
        var find = {
            'id': id,
            'password': pass,
            'smsConfirmation': parseInt(sms)
        }
        global.db.people.find(find, function (err, item) {
            if (item.length == 1) {
                confirmSms(id, res, req);
            } else {
                log('unable to confirm SMS for user ' + id, 400, req.connection.remoteAddress, true);
                res.send(400);
            }
        });
    }
    isIpBlocked(req.connection.remoteAddress)
        .then(doConfirm, function () {
            res.send(403);
        });
}
exports.login = function (req, res) {
    var doLogin = function () {
        var id = req.param("Id");
        var data = req.body;
        var pass = data.password;
        var find = {
            'id': id,
            'password': pass
        }
        global.db.people.find(find, function (err, item) {
            setTimeout(function () {
                if (item.length == 1) {
                    log('user ' + id + ' logged in', 200, req.connection.remoteAddress, false);
                    res.send(200);
                } else {
                    log('login failed for user ' + id, 400, req.connection.remoteAddress, true);
                    res.send(400);
                }
            }, 3000);
        });
    };
    isIpBlocked(req.connection.remoteAddress)
        .then(doLogin, function () {
            res.send(403);
        });
}
exports.resetPassword = function (req, res) {
    var doReset = function () {
        var id = req.param("Id");
        var data = req.body;
        var pass = data.password;
        var sms = data.sms;
        var find = {
            'id': id,
            'smsConfirmation': parseInt(sms)
        }
        global.db.people.find(find, function (err, item) {
            if (item.length == 1) {
                changePassword(id, pass, res, req);
            } else {
                log('unable to change password to user ' + id, 400, req.connection.remoteAddress, true);
                res.send(400);
            }
        });
    }
    isIpBlocked(req.connection.remoteAddress)
        .then(doReset, function () {
            res.send(403);
        });
}
exports.getSms = function (req, res) {
    var id = req.param("Id");
    var sms = Math.floor(Math.random() * 9000) + 1000;
    var find = {
        'id': id
    }
    global.db.people.find(find, function (err, item) {
        if (item.length == 1) {
            global.db.people.update({
                id: id
            }, {
                $set: {
                    smsConfirmation: sms
                }
            });
            sendSms(sms, id, res, req);
        } else {
            log('unable to send SMS to user ' + id, 400, req.connection.remoteAddress, false);
            res.send(400);
        }
    });
}

function addNewRecord(data, res, req, image) {
    global.db.people.insert(data, function (err, result) {
        log('new account created : ' + data.id, 200, req.connection.remoteAddress, true);
        var imgX = image;
        var img = new Buffer(imgX, 'base64');
        var filename = data.systemId + '.png';
        global.s3.putBuffer('/profilesImg/' + filename, img, 'public-read', {}, function (error, result) {
            log('Image ' + filename + ' for account ' + data.id + ' placed in AWS S3', 100, '0.0.0.0', false);
            sendSms(data.smsConfirmation, data.id, res, req);
        });
    });
}

function confirmSms(id, res, req) {
    global.db.people.update({
        id: id
    }, {
        $set: {
            status: 'active',
            lastLogin: new Date()
        }
    }, function (err, result) {;
        log('sms confirmed for ' + id, 200, req.connection.remoteAddress, false);
        res.send(200);
    });
}

function changePassword(id, password, res, req) {
    global.db.people.update({
        id: id
    }, {
        $set: {
            password: password
        }
    }, function (err, result) {;
        log('password changed for ' + id, 200, req.connection.remoteAddress, false);
        res.send(200);
    });
}

function sendSms(code, phone, res, req) {
    var doSend = function () {
        log('SMS sent to ' + phone + ': ' + code, 200, req.connection.remoteAddress, true);
        res.send(200);
        return;
        var path = '/sms/json?api_key=' + global.smsProviderCredentials.key +'&api_secret=' + global.smsProviderCredentials.secret +'&from=Near-X&to=447903450712&text=Your+Near-X+Code:%20' + code;
        console.log('sending sms : ' + path);
        var smsProvider = require('https');
        var options = {
            host: 'rest.nexmo.com',
            port: 443,
            path: path
        };
        smsProvider.request(options, function (response) {
            var str = "";
            response.on('data', function (chunk) {
                str += chunk;
            });
            response.on('end', function () {
                console.log(str);
                resJson = JSON.parse(str);
                console.log(resJson);
                if (resJson.messages[0].status == 0) {
                    log('SMS sent to ' + phone + ': ' + code, 200, req.connection.remoteAddress, true);
                    log('Balance on SMS Provider :  ' + resJson.messages[0]["remaining-balance"], 100, '', false);
                    res.send(200);
                } else {
                    log('Unable to send SMS to ' + phone + ': ' + code + ' -- ' + resJson.messages[0].status, 500, req.connection.remoteAddress, false);
                    res.send(500);
                }
            });
        }).end();
    }
    isIpBlocked(req.connection.remoteAddress)
        .then(doSend, function () {
            res.send(403);
        });
}

function isIpBlocked(ip) {
    var deferred = Q.defer();
    var dt = new Date();
    global.db.system.find({
        ip: ip,
        type: 'blocked',
        blockType: 'perm'
    }, function (err, item) {
        if (item.length > 0) {
            console.log('ip perm blocked');
            deferred.reject(new Error("IP is Blocked"));
        } else {
            global.db.system.find({
                ip: ip,
                type: 'ipBlocked',
                blockType: 'temp',
                blockExp: {
                    $gt: dt
                }
            }, function (err, item2) {
                if (item2.length > 0) {
                    deferred.reject(new Error("IP is Blocked"));
                } else {
                    deferred.resolve(true);
                }
            });
        }
    });
    return deferred.promise;
}

function log(msg, code, ip, watch) {
    var dt = new Date();
    var m = dt + ' | ' + ip + ' | ' + code + ' | ' + msg;
    console.log(m);
    var logRecord = {
        dateTime: dt,
        ip: ip,
        code: code,
        watch: watch,
        message: msg
    };
    global.db.log.insert(logRecord, function (err, result) {
        if (watch) {
            var twoMinutesAgo = new Date(dt - 2 * 60000);
            global.db.log.find({
                ip: ip,
                watch: true,
                dateTime: {
                    $gt: twoMinutesAgo
                }
            }, function (err, item) {
                if (item.length > 10) {
                    var sixtyMinutesAfter = new Date(dt.getTime() + 60 * 60000);
                    var block = {
                        type: 'ipBlocked',
                        ip: ip,
                        dateTime: dt,
                        blockType: 'temp',
                        blockExp: sixtyMinutesAfter
                    }
                    global.db.system.insert(block, function (err, result) {});
                    log("IP " + ip + " blocked temporary for 60 minutes", 100, ip, false);
                }
            });
        }
    });
}