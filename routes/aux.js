exports.query = function (req, res) {
    var subject = req.param("subject");
    var data = req.param("data");
    var find;
    if(subject =='getAreaCode')
    {
        var find = {
        'type': 'country' , 
        'iso': data 
        };
        var fieldsOut = {areaCode: 1, _id:0}
    }
    global.db.aux.find(find, fieldsOut,function (err, item) {
        if (item.length == 1) {
            res.json(200, item[0]);
        }
        else
        {
            res.send(404);
        }
    });
}